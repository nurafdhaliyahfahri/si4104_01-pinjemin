package com.example.login_register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class loginregister_nama extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText etusername, etpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginregister_nama);

        mAuth = FirebaseAuth.getInstance();
        etusername = findViewById(R.id.et_masukannama);
        etpassword = findViewById(R.id.et_mspassword);

        Button btnusername = findViewById(R.id.bt_selanjutnya);
        btnusername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String useruser = etusername.getText().toString();
                String passuser = etpassword.getText().toString();

                if (useruser.equals("")) {
                    Toast.makeText(loginregister_nama.this, "silahkan isi username anda",
                            Toast.LENGTH_SHORT).show();
                } else if (passuser.equals("")) {
                    Toast.makeText(loginregister_nama.this, "silahkan isi password anda",
                            Toast.LENGTH_SHORT).show();
                } else {

                    mAuth.createUserWithEmailAndPassword(useruser, passuser)
                            .addOnCompleteListener(loginregister_nama.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information

                                        FirebaseUser user = mAuth.getCurrentUser();
                                        Toast.makeText(loginregister_nama.this, "Authentication success.",
                                                Toast.LENGTH_SHORT).show();


                                    } else {
                                        // If sign in fails, display a message to the user.

                                        Toast.makeText(loginregister_nama.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                    // ...
                                }
                            });
                }
            }
        });

    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }
}