package com.example.login_register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class loginsebagai extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginsebagai);
    }

    public void launchuserdActivity(View view) {
        Intent intent = new Intent(this, loginregister_user.class);
        startActivity(intent);
    }

    public void launchvendordActivity(View view) {
        Intent intent = new Intent(this, loginregister_vendor.class);
        startActivity(intent);
    }
}
