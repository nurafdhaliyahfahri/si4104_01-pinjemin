package com.example.login_register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class loginregister_vendor extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText etvdpengguna, etvdpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginregister_vendor);

        mAuth = FirebaseAuth.getInstance();
        etvdpengguna = findViewById(R.id.vd_user);
        etvdpass = findViewById(R.id.vd_pass);

        Button btnlogin = findViewById(R.id.vd_masuk);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namauser = etvdpengguna.getText().toString();
                String namapassuser = etvdpass.getText().toString();

                if (namauser.equals("")){
                    Toast.makeText(loginregister_vendor.this, "silahkan masukan username anda", Toast.LENGTH_SHORT).show();

                }else if (namapassuser.equals("")){
                    Toast.makeText(loginregister_vendor.this, "silahkan masukan password anda", Toast.LENGTH_SHORT).show();
                } else {
                    mAuth.signInWithEmailAndPassword(namauser, namapassuser)
                            .addOnCompleteListener(loginregister_vendor.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information

                                        FirebaseUser user = mAuth.getCurrentUser();
                                        Toast.makeText(loginregister_vendor.this, "Authentication success.",
                                                Toast.LENGTH_SHORT).show();

                                    } else {
                                        // If sign in fails, display a message to the user.

                                        Toast.makeText(loginregister_vendor.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                    // ...
                                }
                            });
                }
            }
        });
    }

    public void launchvendordActivity(View view) {
        Intent intent = new Intent(this, loginregister_VdUser.class);
        startActivity(intent);
    }
}
