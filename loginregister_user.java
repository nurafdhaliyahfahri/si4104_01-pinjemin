package com.example.login_register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class loginregister_user extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText etpengguna, etpasspengguna;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginregister_user);
        mAuth = FirebaseAuth.getInstance();

        etpengguna = findViewById(R.id.et_namapengguna);
        etpasspengguna = findViewById(R.id.et_passpengguna);

        Button btnlogin = findViewById(R.id.bt_masuk);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String namauser = etpengguna.getText().toString();
                String namapassuser = etpasspengguna.getText().toString();

                if (namauser.equals("")){
                    Toast.makeText(loginregister_user.this, "silahkan masukan username anda", Toast.LENGTH_SHORT).show();

                }else if (namapassuser.equals("")){
                    Toast.makeText(loginregister_user.this, "silahkan masukan password anda", Toast.LENGTH_SHORT).show();
                } else {
                    mAuth.signInWithEmailAndPassword(namauser, namapassuser)
                            .addOnCompleteListener(loginregister_user.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information

                                        FirebaseUser user = mAuth.getCurrentUser();
                                        Toast.makeText(loginregister_user.this, "Authentication success.",
                                                Toast.LENGTH_SHORT).show();

                                    } else {
                                        // If sign in fails, display a message to the user.

                                        Toast.makeText(loginregister_user.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                    // ...
                                }
                            });
                }
            }
        });

    }

    public void launchuserdActivity(View view) {
        Intent intent = new Intent(this, loginregister_nama.class);
        startActivity(intent);
    }
}
