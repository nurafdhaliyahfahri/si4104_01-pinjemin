package com.example.login_register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class loginregister_VdUser extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText etuser, etpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginregister__vd_user);


        mAuth = FirebaseAuth.getInstance();
        etuser = findViewById(R.id.et_user);
        etpass = findViewById(R.id.et_mspass);

        Button btnvduser = findViewById(R.id.bt_vd_selanjutnya);
        btnvduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uservendor = etuser.getText().toString();
                String passvendor = etpass.getText().toString();


                if (uservendor.equals("")) {
                    Toast.makeText(loginregister_VdUser.this, "silahkan isi username anda",
                            Toast.LENGTH_SHORT).show();
                } else if (passvendor.equals("")) {
                    Toast.makeText(loginregister_VdUser.this, "silahkan isi password anda",
                            Toast.LENGTH_SHORT).show();
                } else {

                    mAuth.createUserWithEmailAndPassword(uservendor, passvendor)
                            .addOnCompleteListener(loginregister_VdUser.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information

                                        FirebaseUser user = mAuth.getCurrentUser();
                                        Toast.makeText(loginregister_VdUser.this, "Authentication success.",
                                                Toast.LENGTH_SHORT).show();


                                    } else {
                                        // If sign in fails, display a message to the user.

                                        Toast.makeText(loginregister_VdUser.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                    // ...
                                }
                            });
                }
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }
}
